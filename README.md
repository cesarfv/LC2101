# LibrePlatform / Structure and Mechanisms / 1U Structure

This is a basic 1U CubeSat structure that can be 3D printed or produced through
CNC milling machines. The structure provides space for a stack of 5 LibreCube
boards or any other CubeSat compatible PC/104 style boards.

Its unique feature is that it allows easy replacement of the board stacks,
without the need to disassemble the structure. Further, all six sides provide
identical mounting for panels, making it extremely modular.

![](docs/assets/picture1.png)

## How to Build

The **/build** folder contains:
- STL files for 3D printing or CNC milling
- Bill of materials (BOM)

For the assembly you need:
- Hexagon Screw Driver 1.5 mm
- Hexagon Screw Driver 2.5 mm

## How to Modify

The **/src** folder contains the [FreeCAD](https://www.freecadweb.org/) source files.

## Contribute

- Issue Tracker: https://gitlab.com/librecube/elements/LC2101/-/issues
- Source Code: https://gitlab.com/librecube/elements/LC2101

To learn more on how to successfully contribute please read the contributing
information in the [LibreCube Guidelines](https://librecube.gitlab.io/).

Want to get involved? Join us at [Matrix](https://app.element.io/#/room/#librecube.org:matrix.org)
or via [Email](mailto:info@librecube.org).

## License

The project is licensed under the CERN Open Hardware license.
See the [LICENSE](./LICENSE.txt) file for details.
